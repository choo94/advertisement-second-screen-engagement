#utils.py
from six.moves import cPickle as pickle
#loading required libraries
# from nltk.tokenize import RegexpTokenizer
from numpy import nan
import numpy as np
#import pickle
#import progressbar
import time
from bs4 import BeautifulSoup
import pandas as pd
import os
import json
import csv
import uuid
import boto3
import glob
import requests
import sys
import errno
import threading
import pyarrow
import datetime
import s3fs
from botocore.exceptions import ClientError
#from sklearn.externals import joblib

s3 = s3fs.S3FileSystem()


def load_data_from_dynamodb(table_name):
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table(table_name)
    response = table.scan()
    dict = []
    dict.extend(response['Items'])

    while 'LastEvaluatedKey' in response:
        response = table.scan(
            ExclusiveStartKey=response['LastEvaluatedKey']
        )
        dict.extend(response['Items'])
    return dict

def save_dict(di_, filename_):
    print(filename_)
    with open(filename_, 'wb') as f:
        pickle.dump(di_, f, protocol=4)
        #joblib.dump(di_, f)

def save_dict_for_public(di_,bucketname_,folderpath_,filename_):
    s3b = boto3.client('s3')
    with open(filename_, 'wb') as f:
        pickle.dump(di_, f, protocol=4)
    s3b.upload_file(filename_,bucketname_,folderpath_+'/'+filename_,ExtraArgs={'ACL': 'public-read'})

def upload_to_s3_without_local_save(dictionary, bucketName, key):
    s3Client = boto3.client('s3')
    serializedObject = pickle.dumps(dictionary)
    s3Client.put_object(Body=serializedObject, Bucket=bucketName, Key=key)

def load_dict(filename_):
    print(filename_)
    with open(filename_, 'rb') as f:
        ret_di = pickle.load(f)
        #ret_di = joblib.load(f)
    return ret_di

def load_dict_from_bucket(bucketpath_,filename_):
    #print(filename_)
    with s3.open(bucketpath_+filename_, mode='rb') as f:
        ret_di = pickle.load(f)
    return ret_di

def delete_s3_bucket(bucketName , subfolderPath):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucketName) #eg recommendscripts
    bucket.objects.filter(Prefix=subfolderPath).delete() #eg personalize/testFolder

def count_items_in_S3_bucket(bucketname, subfolderpath):
    s3 = boto3.resource('s3') # connect to S3
    bucket = s3.Bucket(bucketname)  # get the bucket
    count = 0
    objs = bucket.objects.filter(Prefix=subfolderpath)
    for obj in objs:
        count=count+1
    return count

def preprocess_malay(text):
    #Create a clone of my repository and copy the stop_words folder in site_packages/stop-words
    #https://github.com/babupriyavrat/stop-words
    cleantext = BeautifulSoup(text, "lxml").text
    tokenizer = RegexpTokenizer(r'\w+')
    raw = cleantext.lower()
    tokens = tokenizer.tokenize(raw)
    
    # create malay stop words list
    #malay_stop_words = get_stop_words('ms')
    stopword_tokens = tokenizer.tokenize("malaysian.txt")
    # remove stop words from tokens
    #stopped_tokens = [i for i in tokens if not i in malay_stop_words]
    stopped_tokens = [i for i in tokens if not i in stopword_tokens]
    
    itemarr=[]
    for item in stopped_tokens:
        itemarr.append(item)
    return itemarr

import csv

def getStagingDir(product_name):
    with open('./config/product.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if (str(row['product_name'])==product_name):
                return str(row['staging_dir'])

def load_metadata(file_name='gempak_Article_API_metadata.csv'):
    df =pd.read_csv(file_name, sep='|')
    return df

def generate_pickle(product):
    print("Connecting to Dynamodb")
    items = load_data_from_dynamodb('metadata_radiomalay')
    article_text={}
    temp_dict={}
    tags={} 
    genre={}
    titles={}
    url={}
    print ("Generating pickles... ")
    for item in items:
        if product=='radiomalay':
            article_text[str(item['contentId'])]=str(item['Description'])
            temp_dict[str(item['contentId'])]=str(item['Description'])
            tags[str(item['contentId'])]=str(item.get('tags', []))
            titles[str(item['contentId'])]=str(item['Name'])

    if product=='radiomalay':
        print("saving .pkl file")
        start_time = time.time()
        save_dict(temp_dict,product+'.dict.pkl')
        print("time took to save pickle file for dict: --- %s seconds ---" % (time.time() - start_time))

        print("saving .pkl file")
        start_time = time.time()
        save_dict(article_text,product+'.article.pkl')
        print("time took to save pickle file for article text lookup: --- %s seconds ---" % (time.time() - start_time))
    
        print("saving .pkl file")
        start_time = time.time()
        save_dict(tags,product+'.tags.pkl')
        print("time took to save pickle file for tags: --- %s seconds ---" % (time.time() - start_time))
        
        print("saving .pkl file")
        start_time = time.time()
        save_dict(titles,product+'.titles.pkl')
        print("time took to save pickle file for titles: --- %s seconds ---" % (time.time() - start_time))

#def update_pickle(df,product,path_to_json,json_name):
def update_pickle(product):
    print("Update pickle file")
    items = load_data_from_dynamodb('metadata_radiomalay')
    article_text={}
    temp_dict={}
    tags={} 
    genre={}
    titles={}
    url={}

    # Get current product metadata
    temp_dict =load_dict(product+'.dict.pkl')
    article_text =load_dict(product+'.article.pkl')
    tags = load_dict(product+'.tags.pkl')
    titles = load_dict(product+'.titles.pkl')

    # Fetch latest content to be updated
    latestTimeStamp, latestContentId=getLastUpdated(product)
    latestContent = getContent(str(latestContentId))

    # Update metadata with latest content
    article_text[str(latestContent['contentId'])]=str(latestContent['content'])
    temp_dict[str(latestContent['contentId'])]=str(latestContent['content'])
    tags[str(latestContent['contentId'])]=str(latestContent['tags']).lower().split(',')
    titles[str(latestContent['contentId'])]=str(latestContent['title'])

    save_dict(temp_dict,product+'.dict.pkl')
    start_time = time.time()
    print("time took to save pickle file for dict: --- %s seconds ---" % (time.time() - start_time))

    print("saving .pkl file")
    start_time = time.time()
    save_dict(article_text,product+'.article.pkl')
    print("time took to save pickle file for article text lookup: --- %s seconds ---" % (time.time() - start_time))

    print("saving .pkl file")
    start_time = time.time()
    save_dict(tags,product+'.tags.pkl')
    print("time took to save pickle file for tags: --- %s seconds ---" % (time.time() - start_time))

    print("saving .pkl file")
    start_time = time.time()
    save_dict(titles,product+'.titles.pkl')
    print("time took to save pickle file for titles: --- %s seconds ---" % (time.time() - start_time))

    return latestContent['articleId']

def get_tags(product,content_id):
    tags=load_dict('./'+product+'.tags.pkl')
    return tags[str(content_id)]



def get_article_text(product_name,article_id,remove_html_tags='None'):
    local_article = load_dict(product_name+'.article.pkl')
    if remove_html_tags=='Yes':
        cleantext = BeautifulSoup(local_article[str(article_id)], "lxml").text
        return cleantext
    else:
        return local_article[str(article_id)]

#CHANGES 2 START
csv.field_size_limit(100000000)
def load_modeldata_csv_to_dynamo(csvFile):    
    items = []
    with open(csvFile) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='|')
        for row in reader:
            try:
                data = {}
                data['ArticleCMSID'] = int(row['ArticleCMSID'])
                if row['ArticleBody']:
                    data['ArticleBody'] = row['ArticleBody']
                if row['ArticleTitle']:
                    data['ArticleTitle'] = row['ArticleTitle']
                if row['DocumentPublishFrom']:
                    data['DocumentPublishFrom'] = row['DocumentPublishFrom']
                if row['MainImage']:
                    data['MainImage'] = row['MainImage']
                if row['DocumentTags']:
                    data['DocumentTags'] = row['DocumentTags']
                if row['NodeAlias']:
                    data['NodeAlias'] = row['NodeAlias']
                items.append(data)
            except:
                pass
    return items

def load_categories_into_dynamodb(csvFile):
    items = []
    with open(csvFile) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data = {}
            id = uuid.uuid1() 
            data['id'] = str(row['CMSArticleID'])+ str(row['CategoryList'])
            if row['CMSArticleID']:
                data['ArticleCMSID'] = row['CMSArticleID']
            if row['CategoryList']:
                data['CategoryList'] = row['CategoryList']

            items.append(data)
    return items



def dynamodb_put_items(items, tableName):
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table(tableName)

    i=0
    for item in items:
        try:
            table.put_item(Item=item)
            print("number of records: ", i)
        except ClientError as e:
            print(item)
            print("Getting error for above item")
        i=i+1
    return
#CHANGES 2 END

def upload_file_s3(item, file, modelFilePath):
    filePath = item['modelFiles'] + file
    if modelFilePath:
        file = modelFilePath+file 
    print("file uploaded ==== ", filePath)
    s3_client = boto3.client('s3', region_name='ap-southeast-1')
    with open(file, "rb") as file:
        object = file.read()
        s3_client.put_object(Body=object, Bucket=item['bucketPath'], Key=filePath)

def put_model_files_on_s3(productName):
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('recommendation_configuration')
    response = table.get_item(
        Key={
            'id': 1
        }
    )

    item = response['Item']
    #item= itemData
    #upload pickel files on S3
    upload_file_s3(item, productName+'.'+'article.pkl', '')
    upload_file_s3(item, productName+'.'+'tags.pkl', '')
    upload_file_s3(item, productName+'.'+'titles.pkl', '')
    upload_file_s3(item, productName+'.'+'dict.pkl', '')

    #upload model files on S3
    upload_file_s3(item, productName+'.'+'lsi.simIndex.index', 'models/')
    upload_file_s3(item, productName+'.'+'model.lsi', 'models/')
    upload_file_s3(item, productName+'.'+'model.lsi.projection', 'models/')
    
    #upload corpus files on S3
    upload_file_s3(item, productName+'.'+'corpus.mm', 'corpus/')
    upload_file_s3(item, productName+'.'+'corpus.mm.index', 'corpus/')
    upload_file_s3(item, productName+'.'+'dictionary.saved.lsi', 'corpus/')
    upload_file_s3(item, productName+'_Article_API.parquet.gzip','')


#CHANGE -1 - load similarities instead of models for faster lambda function execution
s3_client = boto3.client('s3', region_name='ap-southeast-1')
def upload_sim_s3(item, file, simFilePath):
    filePath = item['simFiles'] + file
    if simFilePath:
        file = simFilePath+file
        print("file uploaded ==== ", filePath)
        #s3_client = boto3.client('s3', region_name='ap-southeast-1')
        with open(file, "rb") as file:
            object = file.read()
            s3_client.put_object(Body=object, Bucket=item['bucketPath'], Key=filePath)

def upload_similarities(item, file, local_file_path):
    filePath = item['simFiles'] + file
    print("s3 file path", filePath)
    if local_file_path:
        file = local_file_path+file
        print("file uploaded ==== ", file)
        #s3_client = boto3.client('s3', region_name='ap-southeast-1')
        with open(file, "rb") as file:
            object = file.read()
            s3_client.put_object(Body=object, Bucket=item['bucketPath'], Key=filePath)

def put_similarities_on_s3(productName,content_id):
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('recommendation_configuration')
    response = table.get_item(
        Key={
            'id': 1
        }
    )
                   
    item = response['Item']
    #upload pickel files on S3
    upload_sim_s3(item, str(content_id)+'_sim.pkl', 'similarities/')

def put_similarities_on_s3_thread(productName,content_ids):
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('recommendation_configuration')
    response = table.get_item(
        Key={
            'id': 1
        }
    )
                   
    item = response['Item']
    #upload pickel files on S3
    for contentid in content_ids:
        t = threading.Thread(target=upload_sim_s3, args=(item,str(contentid)+'_sim.pkl', 'similarities/')).start()

#CHANGE 1- ends

def read_similarities_files_from_local():
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('recommendation_configuration')
    response = table.get_item(
        Key={
            'id': 1
        }
    )

    item = response['Item']
    s3_client = boto3.client('s3', region_name='ap-southeast-1')

    path = 'similarities/*.pkl'   
    files = glob.glob(path)   
    i=0
    for name in files:
        with open(name, "rb") as file:
            object = file.read()
            s3_client.put_object(Body=object, Bucket=item['bucketPath'], Key=name)
            i=i+1
            print("Total files put on S3 = ", i)
#read_similarities_files_from_local()
def get_configuration_from_dynamo():
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('recommendation_configuration')
    response = table.get_item(
        Key={
            'id': 1
        }
    )
                   
    item = response['Item']
    return item

def load_signle_item_into_dynamo(row):
    items = []
    try:
        data = {}
        data['ArticleCMSID'] = int(row['ArticleCMSID'])
        if row['ArticleBody']:
            data['ArticleBody'] = row['ArticleBody']
        if row['ArticleTitle']:
            data['ArticleTitle'] = row['ArticleTitle']
        if row['DocumentPublishFrom']:
            data['DocumentPublishFrom'] = row['DocumentPublishFrom']
        if row['MainImage']:
            data['MainImage'] = row['MainImage']
        if row['DocumentTags']:
            data['DocumentTags'] = row['DocumentTags']
        if row['NodeAlias']:
            data['NodeAlias'] = row['NodeAlias']
        items.append(data)
    except:
        pass
    return items

def get_categories_for_content(content_id):
    headers = {'Accept': 'application/json'}
    auth = ('reco', 'reco@032018')
    CATEGORY_API_URL = 'http://www.gempak.com/rest/cms.documentcategory'
    CATEGORY_API_PARAMS = '?format=json&Where=DocumentID='

    url = CATEGORY_API_URL + CATEGORY_API_PARAMS + str(content_id)
    response = requests.get(url, headers=headers, auth=auth)
    items=[]
    tempDict={}
    try:
        DocumentCategories=json.loads(str(response.content,'utf-8'))['cms_documentcategories'][0]['CMS_DocumentCategory']
        tempDict['CMSArticleID']=content_id
        tempDict['CategoryList']=[d['CategoryID'] for d in DocumentCategories]
        for category in tempDict['CategoryList']:
            obj = {}
            obj['id'] = str(tempDict['CMSArticleID'])+ str(category)
            obj['ArticleCMSID'] = str(tempDict['CMSArticleID'])
            obj['CategoryList'] = str(category)
            items.append(obj)
    except KeyError:
        pass
    return items

def getContent(contentid):
    #connect to dynamodb
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('awani_metadata')

    #Id = 1 configuration is for article pipeline
    response = table.get_item(
       Key={
            'articleId': contentid
      
           }
    )
    item = response['Item']
    return item

def getLastUpdated(product):
    #connect to dynamodb
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('product')

    #Id = 1 configuration is for article pipeline
    response = table.get_item(
       Key={
            'id': 2
      
           }
    )
    item = response['Item']
    
    LastTimeStamp=item['lastUpdateTimeStamp']
    LastContentFetched=item['lastContentFetched']
    LastTimeStamp=datetime.datetime.strptime(LastTimeStamp,'%d %b %Y %H:%M:%S')
    return LastTimeStamp,LastContentFetched
