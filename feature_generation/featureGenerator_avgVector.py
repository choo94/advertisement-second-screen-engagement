import sys
import pathlib
import joblib
import pickle
import operator
import pandas as pd

from itertools import permutations
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Model, model_from_json

import os
from os import listdir
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array

import csv
import tqdm
import numpy as np
import subprocess
import re

import s3fs
import boto3
from botocore.exceptions import ClientError

from scipy.spatial.distance import cdist
from scipy.spatial.distance import cityblock
from scipy.spatial import distance



tempMin = -10.0
latestMin = 1000
s3 = s3fs.S3FileSystem()
modelType = ""

def load_model(model_path):
    with open(model_path, "r") as loaded_model:
        return model_from_json(loaded_model.read())


def init_model(saved_path):
    # Last hidden layer
    selectedlayer = "mixed10"
    base_modelv3 = InceptionV3(weights="imagenet", include_top=False)
    model = Model(
        inputs=base_modelv3.input, outputs=base_modelv3.get_layer(selectedlayer).output
    )
    model_artifact = model.to_json()
    with open(saved_path, "w") as json_file:
        json_file.write(model_artifact)

def upload_to_s3_without_local_save(dictionary, bucketName, key):
    s3Client = boto3.client('s3')
    serializedObject = pickle.dumps(dictionary)
    s3Client.put_object(Body=serializedObject, Bucket=bucketName, Key=key)

def load_dict_from_bucket(bucketpath_,filename_):
    with s3.open(bucketpath_+filename_, mode='rb') as f:
        ret_di = pickle.load(f)
    return ret_di

def get_advertisement_RedirectUrl(adv_ID):
    print(adv_ID)
    #connect to dynamodb
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table('second_screen_engagement')

    #Id = 1 configuration is for article pipeline
    response = table.get_item(
       Key={
            "advertisement_id": adv_ID
           }
    )
    item = response['Item']
    print(item)
    if item:
        return item["adv_redirect_url"]
    else:
        return False

def save_embedding(data, path):
    with open(path, "wb") as f:
        joblib.dump(data, f)


def load_embedding(path):
    with open(path, "rb") as emb:
        return joblib.load(emb)

def populate_Feature_Vector(dict_item, tableName, bucket_name , filepath):
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
    table = dynamodb.Table(tableName)

    for k,v in dict_item.items():
        try:
            print("key : " + str(k))
            print(v)
            if v.size == 0:
                continue
            else:
                upload_to_s3_without_local_save(dict_item,bucket_name,filepath)
                fullpath = bucket_name+"/"+filepath
                print(fullpath)
                table.put_item(Item={"advertisement_id":k, "adv_feature_vector_s3_loc":fullpath, "adv_redirect_url":"http://testurl/"+ filepath })
                print(k + " uploaded to S3 and DynamoDB")    
        except ClientError as e:
            print(k)
            print("Getting error for above item")
            print(e)
    return

def preprocess_input(x):
    x /= 255.0
    x -= 0.5
    x *= 2.0
    return x


def load_photos_predict(directory, model):
    images = []
    image = None
    for name in directory.glob("*.jpg"):       
        if modelType=="vgg16":
            image = load_img(str(name), target_size=(224, 224)) # load an image from file

        elif modelType=="inceptionv3":
            image = load_img(str(name), target_size=(299, 299)) # load an image from file

        # convert the image pixels to a numpy array
        image = img_to_array(image)
        # print(image.shape) # v3-(299, 299, 3); vgg-(224,224,3)
        # reshape data for the model
        image = np.expand_dims(image, axis=0)
        # print(image.shape) # v3-(1, 299, 299, 3); vgg-(1,224,224,3)

        # prepare the image for the  model
        image = preprocess_input(image)
         # get image id
        feature = model.predict(image).ravel()
        images.append(feature)
    return np.average(images, axis=0)

# Generates feature vectors for advertisement videos
def generate_embedding(model, image_path):
    allEmb = {}
    for filepath in pathlib.Path(image_path).glob("**"):
        emb = {}
        print(filepath)
        assetName = filepath.stem
        print("Video : " + assetName)
        feature = load_photos_predict(filepath, model)
        allEmb[assetName] = feature

        emb[assetName] = feature
        key = assetName+".pkl"
        populate_Feature_Vector(emb,"second_screen_engagement","adv-second-screen-engagement","adv_feature_vector/"+key)
    save_embedding(allEmb, "inceptionv3_all_emb.pkl")

 
# if distance value < threshold, return adv name
def getMatch(threshold, phone_featureVector, adv_vector, distanceType):
    global latestMin
    global tempMin
    if(distanceType == "l1"):
        print(type(phone_featureVector))
        print(adv_vector)
        dist = cityblock(phone_featureVector, adv_vector)

    elif(distanceType == "cosine"):
        dist = distance.cosine(phone_featureVector, adv_vector)

    elif(distanceType == "euclidean"):
        dist = distance.euclidean(phone_featureVector, adv_vector)

    # minimum = np.min(dist)
    minimum = dist
    tempMin = minimum
    if minimum < latestMin:
        latestMin = minimum
        print("Min so far: " + str(latestMin))

    if minimum < threshold:
        print("Below threshold!")
        return True
    else:
        return False

def generate_FeatureVector(model , image, imageLocation):
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    image = preprocess_input(image)
    feature = model.predict(image)
    feature= feature.ravel()
    # print(feature)
    # print(feature.shape)
    return feature

# Get frames from phone video
def checkVideoStream(model , imageLoc):
    print("Getting phone video stream input...")
    with open('adv_namelist.txt') as file:
        advertisementList = [i.strip() for i in file]
        # advertisementList.remove('')

    # pass 1 frame image each time into model
    for filepath in pathlib.Path(imageLoc).glob("**"):
        global latestMin
        latestMin = 1000
        assetName = filepath.stem
        print("\n=== Video : " + assetName + " === ")

        for name in filepath.glob("*.jpg"):       
            if modelType=="vgg16":
                image = load_img(str(name), target_size=(224, 224)) # load an image from file

            elif modelType=="inceptionv3":
                image = load_img(str(name), target_size=(299, 299)) # load an image from file
            
            print(name.stem)
            featureVector = generate_FeatureVector(model, image, filepath)

            # For each each advertisement, compute distance for 1D phone feature vector & adv emb
            for i in advertisementList:
                print(i)
                removeSuffix = re.sub('.pkl' , '', i)
                print(removeSuffix)
                print("Load " + i + " from S3...")
                adv_featureVector = load_dict_from_bucket("s3://adv-second-screen-engagement/", "adv_feature_vector/" + i)
                print(adv_featureVector.keys())
                val = adv_featureVector[removeSuffix]
                foundMatchedAdv = getMatch(0.01, featureVector, val, "l1")
             
                filename = "average_threshold_v2.csv"
                if os.path.exists(filename):
                    append_write = 'a' # append if already exists
                    with open(filename, mode=append_write) as myMin:
                        writer = csv.writer(myMin, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        writer.writerow([assetName , removeSuffix, name.stem, tempMin])
                else:
                    append_write = 'w' # make a new file if not
                    with open(filename, mode=append_write) as myMin:
                        writer = csv.writer(myMin, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        writer.writerow(["phone" , "adv", "frame","min"])
                        writer.writerow([assetName , removeSuffix, name.stem, tempMin])

                if foundMatchedAdv:
                    # get redirect URL from dynamodb based on adv name
                    adv_redirect_url = get_advertisement_RedirectUrl(removeSuffix)
                    if adv_redirect_url == False:
                        print("Fail to get advertisement redirect URL.")
                    
                    return adv_redirect_url

if __name__ == "__main__":
    model, image_path, embedding_path = sys.argv[1:]
    if "vgg16" in model:
        print("VGG16 model in use.")
        model = VGG16()
        modelType = "vgg16"
        embedding_path = "vgg16_" + embedding_path

    elif "inception_v3" in model:
        print("Inception V3 model in use.")
        model = load_model(model)
        modelType = "inceptionv3"
        embedding_path = "inceptionv3_" + embedding_path

    else:
        print("Please enter valid input format: python3 featureGenerator.py <model> <video path> <output pkl file>")
    
    generate_embedding(model, image_path)

    # check phone frame against adv embedding
    url = checkVideoStream(model,"../videos/phone/")
    print(url)
