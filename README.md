# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Proof of Concept : Redirects user to advertisement promotion site upon submitting captured advertisement video from device.

1. Paramater:
	* Frame rate : 2 frame per second.
	* Convolutional neural network model : InceptionV3 or VGG-16
2. Input : phone video frame images(2 images for each second(2fps))
3. Output : Redirected URL


### How do I get set up? ###
Dependencies:

* Tensorflow
* OpenCV
* Inception v3 model
* Python 3

AWS servcies:

* EC2 - VideoModelProd
* S3 - advertisement videos&phone videos, advertisement videos vector files
* DynamoDB - second_screen_engagement

### How to run the code? ###

1. ssh into VideoModelProd EC2

2. Set up Tensorflow environment in EC2:

	* virtualenv --python=/usr/bin/python3 --system-site-packages advertisement_screen_engagement
	* source advertisement_screen_engagement/bin/activate
	* pip3 install --upgrade tensorflow-gpu


3. Set up OpenCV for graphic processing in EC2:

	* pip3 install opencv-contrib-python
	
4. Run the script
	
	To generate frame images:
	
	* scripts/extractFrames.sh

	To get redirected URL:
	
	* python3 featureGenerator.py inception_v3.json ../videos/original/ emb.pkl  OR
	* python3 featureGenerator.py vgg16 ../videos/original/ emb.pkl



### Who do I talk to? ###
* babu-priyavrat_chaturvedi@astro.com.my
* soo-may_choo@astro.com.my