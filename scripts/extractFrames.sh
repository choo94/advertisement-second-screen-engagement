#!/bin/bash

fileDir=$"../videos/recorder"
fps=$"2"



ls ${fileDir}/* | parallel -j0 "mkdir {.}; ffmpeg -i {1} -vf fps=${fps} {.}/frame%04d.jpg -hide_banner"  

