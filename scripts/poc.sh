#generates embeddings
# python3 ../feature_generation/featureGenerator.py inception_v3.json ../videos/original/ emb.pkl
# python3 ../feature_generation/featureGenerator.py vgg16 ../videos/original/ emb.pkl

#compute distance
python3 ../feature_generation/computeDistance.py inceptionv3
# python3 ../feature_generation/computeDistance.py vgg16
